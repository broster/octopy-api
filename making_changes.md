# Making Changes

Making changes to the API is easy.
1. Update [swagger.yaml](swagger.yaml)
2. Rebuild with swagger codegen
```bash
docker run --rm -v ${PWD}:/swagger-api swaggerapi/swagger-codegen-cli generate \
           -i /swagger-api/swagger.yaml -l python -o /swagger-api/
```

## Tips and Tricks

Use this [editor](https://editor.swagger.io/) for making changes to `swagger.yaml`
