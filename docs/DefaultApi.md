# swagger_client.DefaultApi

All URIs are relative to *https://api.octopus.energy/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**electricity_meter_points_mpan_get**](DefaultApi.md#electricity_meter_points_mpan_get) | **GET** /electricity-meter-points/{mpan} | Retrieve the details of a meter-point
[**electricity_meter_points_mpan_meters_serial_number_consumption_get**](DefaultApi.md#electricity_meter_points_mpan_meters_serial_number_consumption_get) | **GET** /electricity-meter-points/{mpan}/meters/{serial_number}/consumption/ | Return a list of kWh consumption values for half-hour periods for a given meter-point and meter
[**gas_meter_points_mprn_get**](DefaultApi.md#gas_meter_points_mprn_get) | **GET** /gas-meter-points/{mprn} | Retrieve the details of a meter-point
[**gas_meter_points_mprn_meters_serial_number_consumption_get**](DefaultApi.md#gas_meter_points_mprn_meters_serial_number_consumption_get) | **GET** /gas-meter-points/{mprn}/meters/{serial_number}/consumption/ | Return a list of kWh consumption values for half-hour periods for a given meter-point and meter
[**products_get**](DefaultApi.md#products_get) | **GET** /products | a list of energy products
[**products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get**](DefaultApi.md#products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get) | **GET** /products/{product_code}/electricity-tariffs/{tariff_code}/day-unit-rates | a list of unit rates and standing charges
[**products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get**](DefaultApi.md#products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get) | **GET** /products/{product_code}/electricity-tariffs/{tariff_code}/night-unit-rates/ | a list of unit rates and standing charges
[**products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get**](DefaultApi.md#products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get) | **GET** /products/{product_code}/electricity-tariffs/{tariff_code}/standard-unit-rates/ | a list of unit rates and standing charges
[**products_product_code_electricity_tariffs_tariff_code_standing_charges_get**](DefaultApi.md#products_product_code_electricity_tariffs_tariff_code_standing_charges_get) | **GET** /products/{product_code}/electricity-tariffs/{tariff_code}/standing-charges | a list of unit rates and standing charges
[**products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get**](DefaultApi.md#products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get) | **GET** /products/{product_code}/gas-tariffs/{tariff_code}/standard-unit-rates | a list of unit rates and standing charges
[**products_product_code_gas_tariffs_tariff_code_standing_charges_get**](DefaultApi.md#products_product_code_gas_tariffs_tariff_code_standing_charges_get) | **GET** /products/{product_code}/gas-tariffs/{tariff_code}/standing-charges | a list of unit rates and standing charges
[**products_product_code_get**](DefaultApi.md#products_product_code_get) | **GET** /products/{product_code} | a list of energy products


# **electricity_meter_points_mpan_get**
> InlineResponse2002 electricity_meter_points_mpan_get(mpan)

Retrieve the details of a meter-point

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
mpan = 'mpan_example' # str | The electricity meter-point’s MPAN

try:
    # Retrieve the details of a meter-point
    api_response = api_instance.electricity_meter_points_mpan_get(mpan)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->electricity_meter_points_mpan_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mpan** | **str**| The electricity meter-point’s MPAN | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **electricity_meter_points_mpan_meters_serial_number_consumption_get**
> InlineResponse2003 electricity_meter_points_mpan_meters_serial_number_consumption_get(mpan, serial_number, period_from=period_from, period_to=period_to, page_size=page_size, order_by=order_by, group_by=group_by)

Return a list of kWh consumption values for half-hour periods for a given meter-point and meter

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
mpan = 'mpan_example' # str | The electricity meter-point’s MPAN
serial_number = 'serial_number_example' # str | The meter's serial number
period_from = 'period_from_example' # str | Show consumption from the given datetime (inclusive). This parameter can be provided on its own (optional)
period_to = 'period_to_example' # str | Show consumption to the given datetime (exclusive). This parameter also requires providing the period_from parameter to create a range (optional)
page_size = 789 # int | Page size of returned results. Default is 100, maximum is 25,000 to give a full year of half-hourly consumption details (optional)
order_by = 'order_by_example' # str | Ordering of results returned. Default is that results are returned in reverse order from latest available figure. (optional)
group_by = 'group_by_example' # str | Grouping of consumption. Default is that consumption is returned in half-hour periods. (optional)

try:
    # Return a list of kWh consumption values for half-hour periods for a given meter-point and meter
    api_response = api_instance.electricity_meter_points_mpan_meters_serial_number_consumption_get(mpan, serial_number, period_from=period_from, period_to=period_to, page_size=page_size, order_by=order_by, group_by=group_by)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->electricity_meter_points_mpan_meters_serial_number_consumption_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mpan** | **str**| The electricity meter-point’s MPAN | 
 **serial_number** | **str**| The meter&#39;s serial number | 
 **period_from** | **str**| Show consumption from the given datetime (inclusive). This parameter can be provided on its own | [optional] 
 **period_to** | **str**| Show consumption to the given datetime (exclusive). This parameter also requires providing the period_from parameter to create a range | [optional] 
 **page_size** | **int**| Page size of returned results. Default is 100, maximum is 25,000 to give a full year of half-hourly consumption details | [optional] 
 **order_by** | **str**| Ordering of results returned. Default is that results are returned in reverse order from latest available figure. | [optional] 
 **group_by** | **str**| Grouping of consumption. Default is that consumption is returned in half-hour periods. | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **gas_meter_points_mprn_get**
> InlineResponse2002 gas_meter_points_mprn_get(mprn)

Retrieve the details of a meter-point

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
mprn = 'mprn_example' # str | The gas meter-point’s MPRN

try:
    # Retrieve the details of a meter-point
    api_response = api_instance.gas_meter_points_mprn_get(mprn)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->gas_meter_points_mprn_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mprn** | **str**| The gas meter-point’s MPRN | 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **gas_meter_points_mprn_meters_serial_number_consumption_get**
> InlineResponse2003 gas_meter_points_mprn_meters_serial_number_consumption_get(mprn, serial_number, period_from=period_from, period_to=period_to, page_size=page_size, order_by=order_by, group_by=group_by)

Return a list of kWh consumption values for half-hour periods for a given meter-point and meter

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
mprn = 'mprn_example' # str | The gas meter-point’s MPRN
serial_number = 'serial_number_example' # str | The meter's serial number
period_from = 'period_from_example' # str | Show consumption from the given datetime (inclusive). This parameter can be provided on its own (optional)
period_to = 'period_to_example' # str | Show consumption to the given datetime (exclusive). This parameter also requires providing the period_from parameter to create a range (optional)
page_size = 789 # int | Page size of returned results. Default is 100, maximum is 25,000 to give a full year of half-hourly consumption details (optional)
order_by = 'order_by_example' # str | Ordering of results returned. Default is that results are returned in reverse order from latest available figure. (optional)
group_by = 'group_by_example' # str | Grouping of consumption. Default is that consumption is returned in half-hour periods. (optional)

try:
    # Return a list of kWh consumption values for half-hour periods for a given meter-point and meter
    api_response = api_instance.gas_meter_points_mprn_meters_serial_number_consumption_get(mprn, serial_number, period_from=period_from, period_to=period_to, page_size=page_size, order_by=order_by, group_by=group_by)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->gas_meter_points_mprn_meters_serial_number_consumption_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mprn** | **str**| The gas meter-point’s MPRN | 
 **serial_number** | **str**| The meter&#39;s serial number | 
 **period_from** | **str**| Show consumption from the given datetime (inclusive). This parameter can be provided on its own | [optional] 
 **period_to** | **str**| Show consumption to the given datetime (exclusive). This parameter also requires providing the period_from parameter to create a range | [optional] 
 **page_size** | **int**| Page size of returned results. Default is 100, maximum is 25,000 to give a full year of half-hourly consumption details | [optional] 
 **order_by** | **str**| Ordering of results returned. Default is that results are returned in reverse order from latest available figure. | [optional] 
 **group_by** | **str**| Grouping of consumption. Default is that consumption is returned in half-hour periods. | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_get**
> InlineResponse200 products_get(is_variable=is_variable, is_green=is_green, is_tracker=is_tracker, is_prepay=is_prepay, is_business=is_business, available_at=available_at)

a list of energy products

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
is_variable = true # bool | Show only variable products (optional)
is_green = true # bool | Show only green products (optional)
is_tracker = true # bool | Show only tracker products (optional)
is_prepay = true # bool | Show only pre-pay products (optional)
is_business = true # bool | Show only business products (optional)
available_at = 'available_at_example' # str | Show products available for new agreements on the given datetime (optional)

try:
    # a list of energy products
    api_response = api_instance.products_get(is_variable=is_variable, is_green=is_green, is_tracker=is_tracker, is_prepay=is_prepay, is_business=is_business, available_at=available_at)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **is_variable** | **bool**| Show only variable products | [optional] 
 **is_green** | **bool**| Show only green products | [optional] 
 **is_tracker** | **bool**| Show only tracker products | [optional] 
 **is_prepay** | **bool**| Show only pre-pay products | [optional] 
 **is_business** | **bool**| Show only business products | [optional] 
 **available_at** | **str**| Show products available for new agreements on the given datetime | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get**
> InlineResponse2001 products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_electricity_tariffs_tariff_code_day_unit_rates_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get**
> InlineResponse2001 products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_electricity_tariffs_tariff_code_night_unit_rates_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get**
> InlineResponse2001 products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_electricity_tariffs_tariff_code_standard_unit_rates_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_electricity_tariffs_tariff_code_standing_charges_get**
> InlineResponse2001 products_product_code_electricity_tariffs_tariff_code_standing_charges_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_electricity_tariffs_tariff_code_standing_charges_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_electricity_tariffs_tariff_code_standing_charges_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get**
> InlineResponse2001 products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_gas_tariffs_tariff_code_standard_unit_rates_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_gas_tariffs_tariff_code_standing_charges_get**
> InlineResponse2001 products_product_code_gas_tariffs_tariff_code_standing_charges_get(product_code, tariff_code)

a list of unit rates and standing charges

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariff_code = 'tariff_code_example' # str | The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A

try:
    # a list of unit rates and standing charges
    api_response = api_instance.products_product_code_gas_tariffs_tariff_code_standing_charges_get(product_code, tariff_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_gas_tariffs_tariff_code_standing_charges_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariff_code** | **str**| The code of the tariff to be retrieved, for example E-1R-VAR-17-01-11-A | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **products_product_code_get**
> DetailedProduct products_product_code_get(product_code, tariffs_active_at=tariffs_active_at)

a list of energy products

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: basicAuth
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
product_code = 'product_code_example' # str | The code of the product to be retrieved, for example VAR-17-01-11
tariffs_active_at = 'tariffs_active_at_example' # str | The point in time in which to show the active charges. Defaults to current datetime (optional)

try:
    # a list of energy products
    api_response = api_instance.products_product_code_get(product_code, tariffs_active_at=tariffs_active_at)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->products_product_code_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_code** | **str**| The code of the product to be retrieved, for example VAR-17-01-11 | 
 **tariffs_active_at** | **str**| The point in time in which to show the active charges. Defaults to current datetime | [optional] 

### Return type

[**DetailedProduct**](DetailedProduct.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

