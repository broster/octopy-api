# Charge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_exc_vat** | **float** |  | [optional] 
**value_inc_vat** | **float** |  | [optional] 
**valid_from** | **str** |  | [optional] 
**valid_to** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


