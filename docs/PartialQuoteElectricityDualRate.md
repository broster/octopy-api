# PartialQuoteElectricityDualRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dual_fuel_single_rate** | **int** |  | [optional] 
**dual_fuel_dual_rate** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


