# FullQuote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direct_debit_monthly** | [**PartialQuote**](PartialQuote.md) |  | [optional] 
**direct_debit_quarterly** | [**PartialQuote**](PartialQuote.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


