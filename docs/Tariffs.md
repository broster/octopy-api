# Tariffs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**a** | [**FullTariff**](FullTariff.md) |  | [optional] 
**b** | [**FullTariff**](FullTariff.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


