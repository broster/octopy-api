# PartialQuote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**electricity_single_rate** | [**PartialQuoteElectricitySingleRate**](PartialQuoteElectricitySingleRate.md) |  | [optional] 
**electricity_dual_rate** | [**PartialQuoteElectricityDualRate**](PartialQuoteElectricityDualRate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


