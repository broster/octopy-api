# PartialTariff

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**standard_unit_rate_exc_vat** | **int** |  | [optional] 
**standard_unit_rate_inc_vat** | **int** |  | [optional] 
**standing_charge_exc_vat** | **int** |  | [optional] 
**standing_charge_inc_vat** | **int** |  | [optional] 
**online_discount_exc_vat** | **int** |  | [optional] 
**online_discount_inc_vat** | **int** |  | [optional] 
**dual_fuel_discount_exc_vat** | **int** |  | [optional] 
**dual_fuel_discount_inc_vat** | **int** |  | [optional] 
**exit_fees_exc_vat** | **int** |  | [optional] 
**exit_fees_inc_vat** | **int** |  | [optional] 
**links** | [**list[Link]**](Link.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


