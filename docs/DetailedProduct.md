# DetailedProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**full_name** | **str** |  | [optional] 
**display_name** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**is_variable** | **bool** |  | [optional] 
**is_green** | **bool** |  | [optional] 
**is_tracker** | **bool** |  | [optional] 
**is_prepay** | **bool** |  | [optional] 
**is_business** | **bool** |  | [optional] 
**is_restricted** | **bool** |  | [optional] 
**term** | **int** |  | [optional] 
**brand** | **str** |  | [optional] 
**available_from** | **str** |  | [optional] 
**available_to** | **str** |  | [optional] 
**links** | [**list[Link]**](Link.md) |  | [optional] 
**tariffs_active_at** | **str** |  | [optional] 
**single_register_electricity_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**dual_register_electricity_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**single_register_gas_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**sample_quotes** | [**ExtraProductSampleQuotes**](ExtraProductSampleQuotes.md) |  | [optional] 
**sample_consumption** | [**ExtraProductSampleConsumption**](ExtraProductSampleConsumption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


