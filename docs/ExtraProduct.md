# ExtraProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tariffs_active_at** | **str** |  | [optional] 
**single_register_electricity_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**dual_register_electricity_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**single_register_gas_tariffs** | [**Tariffs**](Tariffs.md) |  | [optional] 
**sample_quotes** | [**ExtraProductSampleQuotes**](ExtraProductSampleQuotes.md) |  | [optional] 
**sample_consumption** | [**ExtraProductSampleConsumption**](ExtraProductSampleConsumption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


