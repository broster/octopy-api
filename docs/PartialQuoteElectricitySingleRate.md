# PartialQuoteElectricitySingleRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annual_cost_inc_vat** | **int** |  | [optional] 
**annual_cost_exc_vat** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


