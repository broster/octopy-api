# Consumption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumption** | **float** |  | [optional] 
**interval_start** | **str** |  | [optional] 
**interval_end** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


