# ExtraProductSampleConsumption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**electricity_single_rate** | [**ExtraProductSampleConsumptionElectricitySingleRate**](ExtraProductSampleConsumptionElectricitySingleRate.md) |  | [optional] 
**electricity_dual_rate** | [**ExtraProductSampleConsumptionElectricityDualRate**](ExtraProductSampleConsumptionElectricityDualRate.md) |  | [optional] 
**dual_fuel_single_rate** | [**ExtraProductSampleConsumptionDualFuelSingleRate**](ExtraProductSampleConsumptionDualFuelSingleRate.md) |  | [optional] 
**dual_fuel_dual_rate** | [**ExtraProductSampleConsumptionDualFuelDualRate**](ExtraProductSampleConsumptionDualFuelDualRate.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


