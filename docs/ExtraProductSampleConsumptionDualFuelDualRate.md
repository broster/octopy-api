# ExtraProductSampleConsumptionDualFuelDualRate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**electricity_day** | **int** |  | [optional] 
**electricity_night** | **int** |  | [optional] 
**gas_standard** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


