# FullTariff

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direct_debit_monthly** | [**PartialTariff**](PartialTariff.md) |  | [optional] 
**direct_debit_quarterly** | [**PartialTariff**](PartialTariff.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


