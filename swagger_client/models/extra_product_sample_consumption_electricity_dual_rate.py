# coding: utf-8

"""
    Octopus Energy

    This is a representation of the Octopus Energy [REST API](https://developer.octopus.energy/docs/api/)  # noqa: E501

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class ExtraProductSampleConsumptionElectricityDualRate(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'electricity_day': 'int',
        'electricity_night': 'int'
    }

    attribute_map = {
        'electricity_day': 'electricity_day',
        'electricity_night': 'electricity_night'
    }

    def __init__(self, electricity_day=None, electricity_night=None):  # noqa: E501
        """ExtraProductSampleConsumptionElectricityDualRate - a model defined in Swagger"""  # noqa: E501

        self._electricity_day = None
        self._electricity_night = None
        self.discriminator = None

        if electricity_day is not None:
            self.electricity_day = electricity_day
        if electricity_night is not None:
            self.electricity_night = electricity_night

    @property
    def electricity_day(self):
        """Gets the electricity_day of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501


        :return: The electricity_day of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501
        :rtype: int
        """
        return self._electricity_day

    @electricity_day.setter
    def electricity_day(self, electricity_day):
        """Sets the electricity_day of this ExtraProductSampleConsumptionElectricityDualRate.


        :param electricity_day: The electricity_day of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501
        :type: int
        """

        self._electricity_day = electricity_day

    @property
    def electricity_night(self):
        """Gets the electricity_night of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501


        :return: The electricity_night of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501
        :rtype: int
        """
        return self._electricity_night

    @electricity_night.setter
    def electricity_night(self, electricity_night):
        """Sets the electricity_night of this ExtraProductSampleConsumptionElectricityDualRate.


        :param electricity_night: The electricity_night of this ExtraProductSampleConsumptionElectricityDualRate.  # noqa: E501
        :type: int
        """

        self._electricity_night = electricity_night

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ExtraProductSampleConsumptionElectricityDualRate):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
