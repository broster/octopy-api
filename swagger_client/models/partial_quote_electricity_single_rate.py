# coding: utf-8

"""
    Octopus Energy

    This is a representation of the Octopus Energy [REST API](https://developer.octopus.energy/docs/api/)  # noqa: E501

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six


class PartialQuoteElectricitySingleRate(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'annual_cost_inc_vat': 'int',
        'annual_cost_exc_vat': 'int'
    }

    attribute_map = {
        'annual_cost_inc_vat': 'annual_cost_inc_vat',
        'annual_cost_exc_vat': 'annual_cost_exc_vat'
    }

    def __init__(self, annual_cost_inc_vat=None, annual_cost_exc_vat=None):  # noqa: E501
        """PartialQuoteElectricitySingleRate - a model defined in Swagger"""  # noqa: E501

        self._annual_cost_inc_vat = None
        self._annual_cost_exc_vat = None
        self.discriminator = None

        if annual_cost_inc_vat is not None:
            self.annual_cost_inc_vat = annual_cost_inc_vat
        if annual_cost_exc_vat is not None:
            self.annual_cost_exc_vat = annual_cost_exc_vat

    @property
    def annual_cost_inc_vat(self):
        """Gets the annual_cost_inc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501


        :return: The annual_cost_inc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501
        :rtype: int
        """
        return self._annual_cost_inc_vat

    @annual_cost_inc_vat.setter
    def annual_cost_inc_vat(self, annual_cost_inc_vat):
        """Sets the annual_cost_inc_vat of this PartialQuoteElectricitySingleRate.


        :param annual_cost_inc_vat: The annual_cost_inc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501
        :type: int
        """

        self._annual_cost_inc_vat = annual_cost_inc_vat

    @property
    def annual_cost_exc_vat(self):
        """Gets the annual_cost_exc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501


        :return: The annual_cost_exc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501
        :rtype: int
        """
        return self._annual_cost_exc_vat

    @annual_cost_exc_vat.setter
    def annual_cost_exc_vat(self, annual_cost_exc_vat):
        """Sets the annual_cost_exc_vat of this PartialQuoteElectricitySingleRate.


        :param annual_cost_exc_vat: The annual_cost_exc_vat of this PartialQuoteElectricitySingleRate.  # noqa: E501
        :type: int
        """

        self._annual_cost_exc_vat = annual_cost_exc_vat

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PartialQuoteElectricitySingleRate):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
